

#include "Win32CoreAudioContext.h"

#define GD_LOGGER_IMPLEMENTATION
#include "gdLogger.h"

//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

#if !defined(TRUE)
#define TRUE 1
#endif

#if !defined(FALSE)
#define FALSE 0
#endif

// #include <combaseapi.h>

#define win32DebugMessage GDL_DEBUG

#include <assert.h>
#define ASSERT(EXPRESSION, MESSAGE)       assert(EXPRESSION)
#define DEBUG_ASSERT(EXPRESSION, MESSAGE) assert(EXPRESSION)
#define PlatformAllocateMemory(Size)      calloc(1, Size)
#define PlatformFreeMemory(Memory)        free(Memory)

#define ZERO_MEMORY(Memory, Size) memset(Memory, 0, Size)

#define M_PI 3.14159265358979323846264338327950288

//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

typedef HRESULT(__stdcall *PFN_CoCreateInstance)(
    _In_ REFCLSID rclsid,
    _In_opt_ LPUNKNOWN pUnkOuter,
    _In_ DWORD dwClsContext,
    _In_ REFIID riid,
    _COM_Outptr_ _At_(*ppv, _Post_readable_size_(_Inexpressible_(varies)))
        LPVOID FAR *ppv);

typedef HRESULT __stdcall FN_CoInitializeEx(_In_opt_ LPVOID pvReserved,
                                            _In_ DWORD dwCoInit);

typedef FN_CoInitializeEx __stdcall *PFN_CoinitializeEx;

typedef void(__stdcall *PFN_CoUninitialize)(void);

typedef void(__stdcall *PFN_CoTaskMemFree)(_Frees_ptr_opt_ LPVOID pv);

//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

static PFN_CoCreateInstance CoCreateInstanceFN = NULL;
static PFN_CoinitializeEx CoInitializeExFN = NULL;
static PFN_CoUninitialize CoUninitializeFN = NULL;
static PFN_CoTaskMemFree CoTaskMemFreeFN = NULL;

//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

static Bool32 win32LoadCOMFunctions(HMODULE *LibHandleOut);

static Bool32 win32CreateCoreAudioContext(Win32CoreAudioContext *CoreAudioC,
                                          const Win32CoreAudioCreateInfo *CreateInfo);

static void win32DestroyCoreAudioContext(Win32CoreAudioContext *ResultCoreAudioC);

static void win32HelperCoreAudioErrorMessageBox(HRESULT HResult, const char *Caption);

static enum SampleFormatENUM
win32GetSampleFormatFromWaveFormat(WAVEFORMATEX *WaveFormatEXT);
static Bool32 win32IsValidSampleFormat(SampleFormat SampleFormat);
static const char *win32GetSampleFormatString(enum SampleFormatENUM SampleType);

//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

Bool32 win32CreateCoreAudioContext(Win32CoreAudioContext *CoreAudioC,
                                   const Win32CoreAudioCreateInfo *CreateInfo) {

    Win32CoreAudioContext newCoreAudioC = { 0 };

    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

    HRESULT hResult;
    hResult = CoCreateInstanceFN(&CLSID_MMDeviceEnumerator_Instance,
                                 NULL,
                                 CLSCTX_ALL,
                                 &IID_IMMDeviceEnumerator_Instance,
                                 (void **)&newCoreAudioC.DeviceEnumerator);
    if (hResult != S_OK) {

        win32HelperCoreAudioErrorMessageBox(hResult, "Error -- CoCreateInstance");
        goto clean_up_and_return_false;
    }

    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

    hResult = IMMDeviceEnumerator_GetDefaultAudioEndpoint(
        newCoreAudioC.DeviceEnumerator, eRender, eConsole, &newCoreAudioC.Device);

    if (hResult != S_OK) {

        win32HelperCoreAudioErrorMessageBox(
            hResult, "Error -- IMMDeviceEnumerator_GetDefaultAudioEndpoint");
        goto clean_up_and_return_false;
    }

    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

    hResult = IMMDevice_Activate(newCoreAudioC.Device,
                                 &IID_IAudioClient_Instance,
                                 CLSCTX_ALL,
                                 NULL,
                                 (void **)&newCoreAudioC.AudioClient);
    if (hResult != S_OK) {

        win32HelperCoreAudioErrorMessageBox(hResult, "Error -- IMMDevice_Activate");
        goto clean_up_and_return_false;
    }

    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    /* GET THE DEAFULT WAVE FORMAT USED BY THE AUDIO ENGINE */
    /*
        The mix format is the format that the audio engine uses internally
       for digital processing of shared-mode streams. This format is not
       necessarily a format that the audio endpoint device supports. Thus,
       the caller might not succeed in creating an exclusive-mode stream
       with a format obtained by calling GetMixFormat.
    */

    {
        WAVEFORMATEXTENSIBLE *mixFormat = NULL;
        hResult = IAudioClient_GetMixFormat(newCoreAudioC.AudioClient,
                                            (WAVEFORMATEX **)&mixFormat);
        if (hResult != S_OK) {

            win32HelperCoreAudioErrorMessageBox(hResult, "Error -- IMMDevice_Activate");
            goto clean_up_and_return_false;
        }

        newCoreAudioC.MixFormat = *mixFormat;
        CoTaskMemFreeFN(mixFormat);
    }

    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    /* SET THE FORMAT FOR THE AUDIO BUFFER */

    /*
    Information taken from:
    https://learn.microsoft.com/en-us/windows/win32/coreaudio/audclnt-streamflags-xxx-constants

    -------------------------------------
    AUDCLNT_STREAMFLAGS_AUTOCONVERTPCM:

    A channel matrixer and a sample rate converter are inserted as necessary
    to convert between the uncompressed format supplied to
    IAudioClient::Initialize and the audio engine mix format.F

    -------------------------------------
    AUDCLNT_STREAMFLAGS_SRC_DEFAULT_QUALITY:

    When used with AUDCLNT_STREAMFLAGS_AUTOCONVERTPCM, a sample rate
    converter with better quality than the default conversion but with a
    higher performance cost is used. This should be used if the audio is
    ultimately intended to be heard by humans as opposed to other scenarios
    such as pumping silence or populating a meter.
    */

    const DWORD streamFlags = AUDCLNT_STREAMFLAGS_AUTOCONVERTPCM
                              | AUDCLNT_STREAMFLAGS_SRC_DEFAULT_QUALITY;

    newCoreAudioC.SelectedFormat = newCoreAudioC.MixFormat;

    if (win32IsValidSampleFormat(CreateInfo->SampleFormat)
        || CreateInfo->SamplesPerSecond > 0) {

        SampleFormat sampleTypeToUse = CreateInfo->SampleFormat;
        WAVEFORMATEXTENSIBLE waveFormatEX = { 0 };
        WAVEFORMATEX *waveFormat = &waveFormatEX.Format;

        waveFormat->nChannels = 2;
        waveFormat->nSamplesPerSec = CreateInfo->SamplesPerSecond > 0
                                         ? CreateInfo->SamplesPerSecond
                                         : newCoreAudioC.MixFormat.Format.nSamplesPerSec;

        if (sampleTypeToUse == SAMPLE_FORMAT_UNKNOWN) {

            if (waveFormat->nSamplesPerSec <= 48000) {
                sampleTypeToUse = SAMPLE_FORMAT_S16;
            } else {
                sampleTypeToUse = SAMPLE_FORMAT_F32;
            }

        } else if (win32IsValidSampleFormat(CreateInfo->SampleFormat) == FALSE) {

            // Sample format value are invalid.
            goto clean_up_and_return_false;
        }

        if (sampleTypeToUse == SAMPLE_FORMAT_S16) {

            /* To use the 16 bit PCM format */
            waveFormat->wFormatTag = WAVE_FORMAT_PCM;
            waveFormat->wBitsPerSample = 16;

        } else if (sampleTypeToUse == SAMPLE_FORMAT_F32) {

            /* To use the float format */
            waveFormat->wFormatTag = WAVE_FORMAT_IEEE_FLOAT;
            waveFormat->wBitsPerSample = 32;
        }

        waveFormat->nBlockAlign =
            (waveFormat->nChannels * waveFormat->wBitsPerSample) / 8;

        waveFormat->nAvgBytesPerSec =
            waveFormat->nSamplesPerSec * waveFormat->nBlockAlign;

        newCoreAudioC.SelectedFormat = waveFormatEX;
    }

    enum SampleFormatENUM mixFormatType =
        win32GetSampleFormatFromWaveFormat(&newCoreAudioC.MixFormat.Format);

    enum SampleFormatENUM usedFormatType =
        win32GetSampleFormatFromWaveFormat(&newCoreAudioC.SelectedFormat.Format);

    GDL_DEBUG("Mix format type: %s", win32GetSampleFormatString(mixFormatType));
    GDL_DEBUG("Selected format type: %s", win32GetSampleFormatString(usedFormatType));

    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    /* GET THE DEVICE PERIOD*/

    /* The phnsDefaultDevicePeriod parameter specifies the default scheduling
     * period for a shared-mode stream. The phnsMinimumDevicePeriod parameter
     * specifies the minimum scheduling period for an exclusive-mode stream. */

    REFERENCE_TIME defaultDevidePeriod = 0;
    hResult = IAudioClient_GetDevicePeriod(newCoreAudioC.AudioClient,
                                           &defaultDevidePeriod,
                                           NULL);
    if (hResult != S_OK) {

        win32HelperCoreAudioErrorMessageBox(hResult, "Error -- IAudioClient_GetDevicePeriod");
        goto clean_up_and_return_false;
    }

    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    /* CREATE/ALLOCATE THE AUDIO BUFFER */

    // Creates a buffer with a 2 seconds capacity
    CONST REFERENCE_TIME requestedBufferDuration = REF_TIMES_PER_SECOND * 2;

    hResult = IAudioClient_Initialize(newCoreAudioC.AudioClient,
                                      AUDCLNT_SHAREMODE_SHARED,
                                      streamFlags,
                                      requestedBufferDuration,
                                      0,
                                      &newCoreAudioC.SelectedFormat.Format,
                                      NULL);
    if (hResult != S_OK) {

        win32HelperCoreAudioErrorMessageBox(hResult, "Error -- IAudioClient_Initialize");
        goto clean_up_and_return_false;
    }

    // Get the created audio buffer size. Witch can be greater than the
    // 'requestedBufferDuration'
    hResult = IAudioClient_GetBufferSize(newCoreAudioC.AudioClient,
                                         &newCoreAudioC.MaxAudioFrameCount);
    if (hResult != S_OK) {

        win32HelperCoreAudioErrorMessageBox(hResult, "Error -- IAudioClient_GetBufferSize");
        goto clean_up_and_return_false;
    }

    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    /* GET AUDIO RENDER INTERFAFCE */

    hResult = IAudioClient_GetService(newCoreAudioC.AudioClient,
                                      &IID_IAudioRenderClient_Instance,
                                      (void **)&newCoreAudioC.AudioRender);
    if (hResult != S_OK) {

        win32HelperCoreAudioErrorMessageBox(hResult, "Error -- IAudioClient_GetService RenderClient");
        goto clean_up_and_return_false;
    }

    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    /* GET THE IAudioClock INTERFACE */

    hResult = IAudioClient_GetService(newCoreAudioC.AudioClient,
                                      &IID_IAudioClock_instance,
                                      (void **)(&newCoreAudioC.AudioClock));
    if (hResult != S_OK) {

        win32HelperCoreAudioErrorMessageBox(hResult, "Error -- IAudioClient_GetService IAudioClock");
        goto clean_up_and_return_false;
    }

    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    /* GET AUDIO STREAM VOLUME INTERFAFCE */

    hResult = IAudioClient_GetService(newCoreAudioC.AudioClient,
                                      &IID_IAudioStreamVolume_Instance,
                                      (void **)&newCoreAudioC.StreamVolume);
    if (hResult != S_OK) {

        win32HelperCoreAudioErrorMessageBox(hResult, "Error -- IAudioClient_GetService StreamVolume");
        goto clean_up_and_return_false;
    }

    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    /* GET   CHANNEL COUNT WITH STREAM VOLUME */

    u32 channelCount = 0;
    hResult = IAudioStreamVolume_GetChannelCount(newCoreAudioC.StreamVolume,
                                                 &channelCount);
    if (hResult != S_OK) {

        win32HelperCoreAudioErrorMessageBox(hResult, "Error -- IAudioStreamVolume_GetChannelCount");
        goto clean_up_and_return_false;
    }

    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    /* SETTING CHANNELS VOLUMES USING STREAM VOLUME */

    float initialVolume = 1.0;
    for (u32 i = 0; i < channelCount; ++i) {

        hResult = IAudioStreamVolume_SetChannelVolume(newCoreAudioC.StreamVolume,
                                                      i,
                                                      initialVolume);
        if (hResult != S_OK) {

            win32HelperCoreAudioErrorMessageBox(hResult, "Error -- IAudioStreamVolume_GetChannelCount");
            goto clean_up_and_return_false;
        }
    }

    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    /* GET THE STREAM LATENCY */

    REFERENCE_TIME latency;
    // This sets the 'latency' variable to zero and I don't know why
    hResult = IAudioClient_GetStreamLatency(newCoreAudioC.AudioClient, &latency);
    if (hResult != S_OK) {

        win32HelperCoreAudioErrorMessageBox(hResult, "Error -- IAudioClient_GetStreamLatency");
        goto clean_up_and_return_false;
    }

    //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

    newCoreAudioC.IsValid = TRUE;
    (*CoreAudioC) = newCoreAudioC;

    return TRUE;

clean_up_and_return_false:

    win32DestroyCoreAudioContext(&newCoreAudioC);
    return FALSE;
}

void win32DestroyCoreAudioContext(Win32CoreAudioContext *CoreAudioC) {

#define SAFE_RELEASE(punk)                                                     \
    if ((punk) != NULL) {                                                      \
        (punk)->lpVtbl->Release(punk);                                         \
        (punk) = NULL;                                                         \
    }

    SAFE_RELEASE(CoreAudioC->StreamVolume)
    SAFE_RELEASE(CoreAudioC->AudioClock)
    SAFE_RELEASE(CoreAudioC->AudioRender)
    SAFE_RELEASE(CoreAudioC->AudioClient)

    SAFE_RELEASE(CoreAudioC->DeviceEnumerator)
    SAFE_RELEASE(CoreAudioC->Device)

#undef SAFE_RELEASE

    CoreAudioC->IsValid = FALSE;
}

Bool32 win32CoreAudioCBufferWriteStart(Win32CoreAudioContext *CoreAudioC,
                                       const u32 DesiredFrameCount,
                                       win32CoreAudioBufferInfo *BufferWriteInfo) {

    if (CoreAudioC == NULL || BufferWriteInfo == NULL) {
        return FALSE;
    }

    if (CoreAudioC->IsValid == FALSE) {
        return FALSE;
    }

    HRESULT hResult;

    // Padding is how much valid data is queued up in the sound buffer
    // if there's enough padding then we could skip writing more data
    UINT32 padding = 0;
    hResult = IAudioClient_GetCurrentPadding(CoreAudioC->AudioClient, &padding);
    if (hResult != S_OK) {

        win32HelperCoreAudioErrorMessageBox(hResult, "Error -- IAudioClient_GetCurrentPadding");
        return FALSE;
    }

    const u32 availableFrameCount = CoreAudioC->MaxAudioFrameCount - padding;
    u32 acquiredFrameCount = (availableFrameCount > DesiredFrameCount)
                                 ? DesiredFrameCount
                                 : availableFrameCount;

    BYTE *samplesBuffer = NULL;
    hResult = IAudioRenderClient_GetBuffer(CoreAudioC->AudioRender,
                                           acquiredFrameCount,
                                           &samplesBuffer);

    if (hResult != S_OK) {

        win32HelperCoreAudioErrorMessageBox(hResult, "Error -- IAudioRenderClient_GetBuffer");
        return FALSE;
    }

    BufferWriteInfo->CoreAudioC = CoreAudioC;
    BufferWriteInfo->BytesPerFrame = CoreAudioC->SelectedFormat.Format.nBlockAlign;
    BufferWriteInfo->FrameCount = acquiredFrameCount;
    BufferWriteInfo->Data = samplesBuffer;
    BufferWriteInfo->DataSize =
        BufferWriteInfo->FrameCount * BufferWriteInfo->BytesPerFrame;

    return TRUE;
}

void win32CoreAudioCBufferWriteEnd(win32CoreAudioBufferInfo *BufferWriteInfo,
                                   Bool32 PlaySilence) {

    if (BufferWriteInfo != NULL && BufferWriteInfo->CoreAudioC != NULL
        && BufferWriteInfo->CoreAudioC->IsValid) {

        /*
           If the AUDCLNT_BUFFERFLAGS_SILENT flag bit is set, the audio engine
           treats the data packet as though it contains silence regardless of the
           data values contained in the packet. This flag eliminates the need for
           the client to explicitly write silence data to the rendering buffer.
        */

        const DWORD dwFlags =
            (PlaySilence == TRUE) ? AUDCLNT_BUFFERFLAGS_SILENT : 0;

        HRESULT hResult;
        hResult = IAudioRenderClient_ReleaseBuffer(BufferWriteInfo->CoreAudioC->AudioRender,
                                                   BufferWriteInfo->FrameCount,
                                                   dwFlags);
        BufferWriteInfo->CoreAudioC = NULL;
        BufferWriteInfo->Data = NULL;

        if (hResult != S_OK) {

            win32HelperCoreAudioErrorMessageBox(hResult, "Error -- IAudioRenderClient_ReleaseBuffer");
            return;
        }
    }
}

Bool32 win32LoadCOMFunctions(HMODULE *LibHandleOut) {

    HMODULE libHandleOle32 = LoadLibraryA("Ole32.dll");
    if (libHandleOle32 != NULL) {

        CoCreateInstanceFN =
            (PFN_CoCreateInstance)(void *)GetProcAddress(libHandleOle32,
                                                         "CoCreateInstance");
        if (CoCreateInstanceFN == NULL) {

            return FALSE;
        }

        CoInitializeExFN =
            (PFN_CoinitializeEx)(void *)GetProcAddress(libHandleOle32, "CoInitializeEx");
        if (CoInitializeExFN == NULL) {

            return FALSE;
        }

        CoUninitializeFN =
            (PFN_CoUninitialize)(void *)GetProcAddress(libHandleOle32, "CoUninitialize");
        if (CoUninitializeFN == NULL) {

            return FALSE;
        }

        CoTaskMemFreeFN =
            (PFN_CoTaskMemFree)(void *)GetProcAddress(libHandleOle32, "CoTaskMemFree");
        if (CoTaskMemFreeFN == NULL) {

            return FALSE;
        }
    }

    HRESULT hResult;
    hResult = CoInitializeExFN(0, COINIT_SPEED_OVER_MEMORY);
    if (hResult != S_OK) {

        const char *messageBoxCaption = "Error -- CoInitializeEx";
        char *messageBoxText = "Unknown";

        switch (hResult) {

            case S_FALSE: {
                messageBoxCaption =
                    "The COM library is already initialized on this thread";
            } break;

            case RPC_E_CHANGED_MODE: {
                messageBoxCaption =
                    "A previous call to CoInitializeEx specified the concurrency model "
                    "for this thread as multithread apartment (MTA). This could also "
                    "indicate that a change from neutral-threaded apartment to "
                    "single-threaded apartment has occurred. ";
            } break;

            default: {

                messageBoxCaption = "Unknown error";
            };
        }

        MessageBoxA(NULL, messageBoxText, messageBoxCaption, MB_OK);
        return FALSE;
    }

    if (LibHandleOut != NULL) {
        *LibHandleOut = libHandleOle32;
    }

    return TRUE;
}

void win32HelperCoreAudioErrorMessageBox(HRESULT HResult, const char *Caption) {

    const char *messageBoxText = "Unknown";
    const char *messageBoxCaption = (Caption == NULL) ? "Error" : Caption;

    if (HResult == S_OK) {
        return;

    } else if (HResult == E_NOTFOUND) {
        messageBoxText = "E_NOTFOUND";
    }

    switch (HResult) {

        case S_FALSE: {
            messageBoxText = "S_FALSE";
        } break;

        case E_INVALIDARG: {
            messageBoxText = "E_INVALIDARG";
        } break;

        case E_OUTOFMEMORY: {
            messageBoxText = "E_OUTOFMEMORY";
        } break;

        case E_NOINTERFACE: {
            messageBoxText = "E_NOINTERFACE";
        } break;

        case AUDCLNT_E_NOT_STOPPED: {
            messageBoxText = "AUDCLNT_E_NOT_STOPPED";
        } break;

        case AUDCLNT_E_DEVICE_INVALIDATED: {
            messageBoxText = "AUDCLNT_E_DEVICE_INVALIDATED";
        } break;

        case AUDCLNT_E_ALREADY_INITIALIZED: {
            messageBoxText = "AUDCLNT_E_ALREADY_INITIALIZED";
        } break;

        case AUDCLNT_E_WRONG_ENDPOINT_TYPE: {
            messageBoxText = "AUDCLNT_E_WRONG_ENDPOINT_TYPE";
        } break;

        case AUDCLNT_E_BUFFER_SIZE_NOT_ALIGNED: {
            messageBoxText = "AUDCLNT_E_BUFFER_SIZE_NOT_ALIGNED";
        } break;

        case AUDCLNT_E_BUFFER_SIZE_ERROR: {
            messageBoxText = "AUDCLNT_E_BUFFER_SIZE_ERROR";
        } break;

        case AUDCLNT_E_CPUUSAGE_EXCEEDED: {
            messageBoxText = "AUDCLNT_E_CPUUSAGE_EXCEEDED";
        } break;

        case AUDCLNT_E_DEVICE_IN_USE: {
            messageBoxText = "E_INVALIDARG";
        } break;

        case AUDCLNT_E_ENDPOINT_CREATE_FAILED: {
            messageBoxText = "AUDCLNT_E_ENDPOINT_CREATE_FAILED";
        } break;

        case AUDCLNT_E_INVALID_DEVICE_PERIOD: {
            messageBoxText = "AUDCLNT_E_INVALID_DEVICE_PERIOD";
        } break;

        case AUDCLNT_E_UNSUPPORTED_FORMAT: {
            messageBoxText = "AUDCLNT_E_UNSUPPORTED_FORMAT";
        } break;

        case AUDCLNT_E_EXCLUSIVE_MODE_NOT_ALLOWED: {
            messageBoxText = "AUDCLNT_E_EXCLUSIVE_MODE_NOT_ALLOWED";
        } break;

        case AUDCLNT_E_BUFDURATION_PERIOD_NOT_EQUAL: {
            messageBoxText = "AUDCLNT_E_BUFDURATION_PERIOD_NOT_EQUAL";
        } break;

        case AUDCLNT_E_SERVICE_NOT_RUNNING: {
            messageBoxText = "AUDCLNT_E_SERVICE_NOT_RUNNING";
        } break;

        case E_UNEXPECTED: {
            messageBoxText = "E_UNEXPECTED";
        } break;

        case REGDB_E_CLASSNOTREG: {
            messageBoxText = "REGDB_E_CLASSNOTREG";
        } break;

        case CLASS_E_NOAGGREGATION: {
            messageBoxText = "CLASS_E_NOAGGREGATION";
        } break;

        case AUDCLNT_E_NOT_INITIALIZED: {
            messageBoxText = "AUDCLNT_E_NOT_INITIALIZED";
        } break;

        case RPC_E_CHANGED_MODE: {
            messageBoxText = "RPC_E_CHANGED_MODE";
        } break;

        case E_POINTER: {
            messageBoxText = "E_POINTER";
        } break;

        default: {
            messageBoxText = "Unknown error";
        }
    }

    MessageBoxA(NULL, messageBoxText, messageBoxCaption, MB_OK);
}

enum SampleFormatENUM win32GetSampleFormatFromWaveFormat(WAVEFORMATEX *waveFormat) {

    if (waveFormat->wFormatTag == WAVE_FORMAT_EXTENSIBLE) {

        static const GUID WAVE_EXTENDED_SUBTYPE_IEEE_FLOAT = {
            (USHORT)(0x0003),
            0x0000,
            0x0010,
            {0x80, 0x00, 0x00, 0xaa, 0x00, 0x38, 0x9b, 0x71},
        };

        static const GUID WAVE_EXTENDED_SUBTYPE_PCM = {
            (USHORT)(1),
            0x0000,
            0x0010,
            {0x80, 0x00, 0x00, 0xaa, 0x00, 0x38, 0x9b, 0x71},
        };

        WAVEFORMATEXTENSIBLE *waveFormatEXT = (WAVEFORMATEXTENSIBLE *)waveFormat;

        if (memcmp(&waveFormatEXT->SubFormat,
                   &WAVE_EXTENDED_SUBTYPE_PCM,
                   sizeof(waveFormatEXT->SubFormat))
            == 0) {

            if (waveFormat->wBitsPerSample == 16) {

                return SAMPLE_FORMAT_S16;

            } else {

                return SAMPLE_FORMAT_UNKNOWN;
            }

        } else if (memcmp(&waveFormatEXT->SubFormat,
                          &WAVE_EXTENDED_SUBTYPE_IEEE_FLOAT,
                          sizeof(waveFormatEXT->SubFormat))
                   == 0) {

            return SAMPLE_FORMAT_F32;
        }

    } else if (waveFormat->wFormatTag == WAVE_FORMAT_PCM) {

        if (waveFormat->wBitsPerSample == 16) {

            return SAMPLE_FORMAT_S16;

        } else {

            return SAMPLE_FORMAT_UNKNOWN;
        }

    } else if (waveFormat->wFormatTag == WAVE_FORMAT_IEEE_FLOAT) {

        return SAMPLE_FORMAT_F32;
    }

    return SAMPLE_FORMAT_UNKNOWN;
}

inline Bool32 win32IsValidSampleFormat(SampleFormat SampleFormat) {

    return (SampleFormat < SAMPLE_FORMAT_MAX_COUNT
            && SampleFormat != SAMPLE_FORMAT_UNKNOWN);
}

const char *win32GetSampleFormatString(enum SampleFormatENUM SampleType) {

    static const char *sampleTypeStrings[] = {

        [SAMPLE_FORMAT_UNKNOWN] = "SAMPLE_FORMAT_UNKNOWN",
        [SAMPLE_FORMAT_S16] = "SAMPLE_FORMAT_S16",
        [SAMPLE_FORMAT_F32] = "SAMPLE_FORMAT_F32",
    };

    return sampleTypeStrings[SampleType];
}

/*
You can choose one of these 2 licenses:
-------------------------------------------------------------------------------------
Alternative A: The MIT License (MIT)
Copyright © 2023 Daniel Martins da Silva

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

-------------------------------------------------------------------------------------
Alternative B: BSD Zero Clause License (0BSD)
Copyright (C) 2023 by Daniel Martins da Silva <daaniel_martins@hotmail.com>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
*/
