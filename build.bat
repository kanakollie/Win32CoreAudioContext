@echo off
:: cls

:: -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ::

:: Name that will be used as base for resulting files from the compilation process
set BaseName=main

set SourceFileName=%BaseName%.c
set ObjectFileName=%BaseName%.obj
set ExecutableFileName=%BaseName%.exe

:: -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ::

set SourcePath=.\
set WorkPath=.\work
set OutputPath=.\build

set ObjectFilePath=%WorkPath%
set ExecutableFilePath=%OutputPath%

:: -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ::

:: File that will be compiled
set SourceFile=%SourcePath%\%SourceFileName%

:: Object file generated from the set %SourceFile%
set ObjectFile=%ObjectFilePath%\%ObjectFileName%

:: Object file generated from the set %ObjectFile%
set ExecutableFile=%ExecutableFilePath%\%ExecutableFileName%

:: -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ::
:: -- EXIT IF SOURCE FILE DO NOT EXIST -- ::
if not exist %SourceFile% exit /b -2

:: -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ::
:: -- CHECK REQUIRED DIRECTORIES -- ::

if not exist %WorkPath% mkdir %WorkPath%
if not exist %OutputPath% mkdir %OutputPath%

if not exist %ObjectFilePath% mkdir %ObjectFilePath%
if not exist %ExecutableFilePath% mkdir %ExecutableFilePath%

:: -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ::
:: -- COMPILER FLAGS -- ::

:: Will disable warnings on the compiler
set WarningToDisableCL=-external:anglebrackets -external:W0 -wd4711 -wd4464 -wd4774 -wd4996 -wd4244 -wd5045 -wd4061 -wd4710

:: The c standard that will be used by the compiler
set CStarndard=-std:c17
set Macros=
IF [%1] == [release] (
   
    ECHO -------- RELEASE --------
    set Macros=%Macros%
    set CompilerFlags=-O2

) ELSE (

    ECHO -------- DEBUG --------
    set Macros=%Macros%
    set CompilerFlags=-Od -Zi -Fd:%OutputPath%\%BaseName%
)

:: Paths containing header files
set IncludePaths=-external:I.\code\external -I.\code\include

:: All flags that will be passed to the compiler
set CompilerFlags=-nologo -c -Wall -GS- %CompilerFlags% -Fo:%ObjectFile% %CStarndard% %SourceFile% %Macros% %IncludePaths% %WarningToDisableCL%

:: -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ::
:: -- LINK FLAGS -- ::

set LinkerWarningsToDisable=
set Arquitecture=-MACHINE:X64
set LinkerFlags=-ILK:%WorkPath%\%BaseName%.ilk

IF [%1] NEQ [release] (

    set LinkerFlags=%LinkerFlags% -DEBUG:FULL -PDB:%ExecutableFilePath%\
)

:: libraries used to compile the %SourceFile%
set Libraries=User32.lib
set StaticLibraryFiles=

:: Paths used to search for libraries
set LibraryPaths=-LIBPATH:.\libs

:: All flags that will be passed to the linker
set LinkerFlags=-nologo -TIME %LinkerFlags% -OUT:%ExecutableFile% -IMPLIB:%WorkPath%\%BaseName%.lib %ObjectFile% %StaticLibraryFiles% %Libraries% %LibraryPaths% %LinkerWarningsToDisable% %Arquitecture%
:: -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ::
:: -- RUN COMMANDS -- ::

cl %CompilerFlags%
if %errorlevel% neq 0 exit /b %errorlevel%

link %LinkerFlags%
if %errorlevel% neq 0 exit /b %errorlevel%
echo: 

:: -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ::

:: You can choose one of these 2 licenses:
:: -------------------------------------------------------------------------------------
:: Alternative A: The MIT License (MIT)    
:: Copyright (C) 2023 Daniel Martins da Silva

:: Permission is hereby granted, free of charge, to any person obtaining a copy
:: of this software and associated documentation files (the "Software"), to deal
:: in the Software without restriction, including without limitation the rights
:: to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
:: copies of the Software, and to permit persons to whom the Software is
:: furnished to do so, subject to the following conditions:

:: The above copyright notice and this permission notice shall be included in all
:: copies or substantial portions of the Software.

:: THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
:: IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
:: FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
:: AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
:: LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
:: OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
:: SOFTWARE.

:: -------------------------------------------------------------------------------------
:: Alternative B: BSD Zero Clause License (0BSD)    
:: Copyright (C) 2023 by Daniel Martins da Silva <daaniel_martins@hotmail.com>

:: Permission to use, copy, modify, and/or distribute this software for any
:: purpose with or without fee is hereby granted.

:: THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
:: REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
:: AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
:: INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
:: LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
:: OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
:: PERFORMANCE OF THIS SOFTWARE.
