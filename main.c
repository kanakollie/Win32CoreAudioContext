#if !defined(WIN32_LEAN_AND_MEAN)
#define WIN32_LEAN_AND_MEAN
#endif

#if !defined(VC_EXTRALEAN)
#define VC_EXTRALEAN
#endif

#if !defined(NOMINMAX)
#define NOMINMAX
#endif

#include <math.h>

#include <Windows.h>
#include "Win32CoreAudioContext.c"

//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

static void GenerateSineSamples16BitPCM(s16 *Samples,
                                        u32 SampleCount,
                                        u32 ChannelCount,
                                        u32 SamplesPerSecond,
                                        double *PlayBackTime);

static void GenerateSineSamplesFloat(float *Samples,
                                     u32 SampleCount,
                                     u32 ChannelCount,
                                     u32 SamplesPerSecond,
                                     double *PlayBackTime);

static Bool32
win32PlaySineWaveTest(Win32CoreAudioContext *CoreAudio, u32 DurationInSeconds);

//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

int main(void) {

    Bool32 resultB = FALSE;
    Win32CoreAudioContext coreAudioC = { 0 };
    Win32CoreAudioCreateInfo coreAudioCCreateInfo = { 0 };
    HMODULE libHandleOle32 = NULL;

    resultB = win32LoadCOMFunctions(&libHandleOle32);
    if (resultB == FALSE) {

        MessageBoxA(NULL, "Failed to start COM interface. Exiting...", "Error", MB_OK);
        return -1;
    }

    // coreAudioCCreateInfo.SamplesPerSecond = 192000;
    coreAudioCCreateInfo.SamplesPerSecond = 48000;

    coreAudioCCreateInfo.SampleFormat = SAMPLE_FORMAT_S16;
    // coreAudioCCreateInfo.SampleFormat = SAMPLE_FORMAT_F32;

    resultB = win32CreateCoreAudioContext(&coreAudioC, &coreAudioCCreateInfo);
    if (resultB == FALSE) {

        win32DebugMessage("Failed to create core audio context");
    }

    if (coreAudioC.IsValid == TRUE) {

        win32DebugMessage("Playing sine wave test sound!");
        resultB = win32PlaySineWaveTest(&coreAudioC, 2);
        if (resultB == FALSE) {

            win32DebugMessage("Failed to play sine wave sound. Exiting");
            return -2;
        }
    }

    // win32DebugMessage("Press enter to exit");
    // getchar();

    win32DestroyCoreAudioContext(&coreAudioC);
    CoUninitializeFN();
    return 0;
}

//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

void GenerateSineSamples16BitPCM(s16 *Samples,
                                 u32 SampleCount,
                                 u32 ChannelCount,
                                 u32 SamplesPerSecond,
                                 double *StartPlayBackTime) {

    const float toneHZ = 512;
    const s16 ToneVolume = 1500;
    double playBackTime = (StartPlayBackTime != NULL ? *StartPlayBackTime : 0);

    for (UINT32 frameIndex = 0; frameIndex < SampleCount; ++frameIndex) {

        float amplitude = (float)sin(playBackTime * 2 * M_PI * toneHZ);
        s16 sample = (s16)(ToneVolume * amplitude);
        // s16 y = sin(2 * M_PI * TONE_HZ * SampleCount / SamplesPerSecond);

        for (u32 j = 0; j < ChannelCount; ++j) {

            *Samples++ = sample;
        }

        playBackTime += 1.f / SamplesPerSecond;
    }

    if (StartPlayBackTime) {
        (*StartPlayBackTime) = playBackTime;
    }
}

void GenerateSineSamplesFloat(float *Samples,
                              u32 SampleCount,
                              u32 ChannelCount,
                              u32 SamplesPerSecond,
                              double *StartPlayBackTime) {

    const float toneHZ = 512;
    const float toneVolume = 0.02f;
    double playBackTime = (StartPlayBackTime != NULL ? *StartPlayBackTime : 0);

    for (u32 sampleIndex = 0; sampleIndex < SampleCount; ++sampleIndex) {

        float amplitude = (float)sin(playBackTime * 2 * M_PI * toneHZ);
        float sample = (float)(toneVolume * amplitude);

        for (u32 j = 0; j < ChannelCount; ++j) {

            *Samples++ = sample;
        }

        playBackTime += 1.f / SamplesPerSecond;
    }

    if (StartPlayBackTime) {
        (*StartPlayBackTime) = playBackTime;
    }
}

Bool32 win32PlaySineWaveTest(Win32CoreAudioContext *CAC, u32 DurationInSeconds) {

    HRESULT hResult;

    WAVEFORMATEX *waveFormat = &CAC->SelectedFormat.Format;
    enum SampleFormatENUM waveSampleType =
        win32GetSampleFormatFromWaveFormat(waveFormat);

    if (waveSampleType == SAMPLE_FORMAT_UNKNOWN) {

        GDL_ERROR("Unsuported audio format");
        return FALSE;
    }

    hResult = IAudioClient_Start(CAC->AudioClient);
    if (hResult != S_OK) {

        win32HelperCoreAudioErrorMessageBox(hResult, "Error -- IAudioClient_Start");
        goto clean_up_and_return_false;
    }

    // Uset to get playback cursor position
    // This is good for visualising playback and seeing the reading/writing in action!
    IAudioClock *audioClock = CAC->AudioClock;

    double playbackTime = 0.0;
    Bool32 playing = TRUE;

    while (playing) {

        // How much of our sound buffer we want to fill on each update.
        // Needs to be enough so that the playback doesn't reach garbage data
        // but we get less latency the lower it is (e.g. how long does it take
        // between pressing jump and hearing the sound effect)
        // Try setting this to e.g. bufferSizeInFrames / 250 to hear what
        // happens when we're not writing enough data to stay ahead of playback!
        UINT32 soundBufferLatencyInFrames = CAC->MaxAudioFrameCount / 50;
        win32CoreAudioBufferInfo audioBuffer = { 0 };

        Bool32 resultB =
            win32CoreAudioCBufferWriteStart(CAC, soundBufferLatencyInFrames, &audioBuffer);
        if (resultB) {

            if (waveSampleType == SAMPLE_FORMAT_S16 && waveFormat->wBitsPerSample == 16) {

                GenerateSineSamples16BitPCM(audioBuffer.Data,
                                            audioBuffer.FrameCount,
                                            waveFormat->nChannels,
                                            waveFormat->nSamplesPerSec,
                                            &playbackTime);

            } else if (waveSampleType == SAMPLE_FORMAT_F32) {

                GenerateSineSamplesFloat(audioBuffer.Data,
                                         audioBuffer.FrameCount,
                                         waveFormat->nChannels,
                                         waveFormat->nSamplesPerSec,
                                         &playbackTime);
            }

            win32CoreAudioCBufferWriteEnd(&audioBuffer, FALSE);

            UINT64 audioPlaybackFreq;
            UINT64 audioPlaybackPos;

            hResult = IAudioClock_GetFrequency(audioClock, &audioPlaybackFreq);
            if (hResult != S_OK) {

                win32HelperCoreAudioErrorMessageBox(hResult, "Error -- IAudioClock_GetFrequency");
                goto clean_up_and_return_false;
            }

            hResult = IAudioClock_GetPosition(audioClock, &audioPlaybackPos, 0);
            if (hResult != S_OK) {

                win32HelperCoreAudioErrorMessageBox(hResult, "Error -- IAudioClock_GetPosition");
                goto clean_up_and_return_false;
            }

            r64 audioPlaybackPosInSeconds =
                (r64)audioPlaybackPos / (r64)audioPlaybackFreq;
            u64 audioPlaybackPosInSamples =
                audioPlaybackPosInSeconds * waveFormat->nSamplesPerSec;

            if (audioPlaybackPosInSeconds >= DurationInSeconds) {

                GDL_DEBUG("Played Samples: %llu", audioPlaybackPosInSamples);
                GDL_DEBUG("Playback time: %f s", audioPlaybackPosInSeconds);

                playing = FALSE;
            }
        }
    }

    return TRUE;

clean_up_and_return_false:

    win32DestroyCoreAudioContext(CAC);
    return FALSE;
}

/*
You can choose one of these 2 licenses:
-------------------------------------------------------------------------------------
Alternative A: The MIT License (MIT)
Copyright © 2023 Daniel Martins da Silva

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

-------------------------------------------------------------------------------------
Alternative B: BSD Zero Clause License (0BSD)
Copyright (C) 2023 by Daniel Martins da Silva <daaniel_martins@hotmail.com>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
*/
