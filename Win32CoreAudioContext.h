#if !defined(WIN32_CORE_AUDIO_CONTEXT)
#define WIN32_CORE_AUDIO_CONTEXT

//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

#define COBJMACROS
#include <Audioclient.h>
#include <endpointvolume.h>
#include <mmdeviceapi.h>

//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

#include <stdint.h>

typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef float r32;
typedef double r64;

typedef intptr_t sPtr;
typedef uintptr_t uPtr;

typedef u32 Bool32;
typedef u32 Flags;

typedef u8 String8;

//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

// NOTE: Number of REFERENCE_TIME units per second
// One unit is equal to 100 nano seconds
#define REF_TIMES_PER_SECOND  10000000
#define REF_TIMES_PER_MSECOND 10000

//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

typedef enum SampleFormatENUM {

    SAMPLE_FORMAT_UNKNOWN,

    // This is a signed 16-bit integer ranging  from -32768 to 32767
    SAMPLE_FORMAT_S16,

    // This is a 32-bit float ranging from -1.0 to 1.0
    SAMPLE_FORMAT_F32,

    SAMPLE_FORMAT_MAX_COUNT,

} SampleFormat;

typedef struct Win32CoreAudioContextSTR {

    IMMDevice *Device;
    IMMDeviceEnumerator *DeviceEnumerator;

    IAudioClient *AudioClient;
    IAudioRenderClient *AudioRender;
    IAudioStreamVolume *StreamVolume;

    //  Used to monitor the stream data rate and the current position in the stream
    IAudioClock *AudioClock;

    WAVEFORMATEXTENSIBLE MixFormat;
    WAVEFORMATEXTENSIBLE SelectedFormat;

    u32 MaxAudioFrameCount;
    Bool32 IsValid;

} Win32CoreAudioContext;

typedef struct Win32CoreAudioCreateInfoSTR {

    u32 SamplesPerSecond;
    SampleFormat SampleFormat;

} Win32CoreAudioCreateInfo;

typedef struct win32CoreAudioBufferInfoSTR {

    Win32CoreAudioContext *CoreAudioC;

    u32 BytesPerFrame;
    u32 FrameCount;

    u64 Offset;

    void *Data;
    // Data size in bytes
    u64 DataSize;

} win32CoreAudioBufferInfo;

//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

static const IID IID_IMMDeviceEnumerator_Instance = {
  /* A95664D2-9614-4F35-A746-DE8DB63617E6 = __uuidof(IMMDeviceEnumerator)  */

    0xA95664D2,
    0x9614,
    0x4F35,
    {0xA7, 0x46, 0xDE, 0x8D, 0xB6, 0x36, 0x17, 0xE6},
};

static const CLSID CLSID_MMDeviceEnumerator_Instance = {
  /* BCDE0395-E52F-467C-8E3D-C4579291692E = __uuidof(MMDeviceEnumerator)  */

    0xBCDE0395,
    0xE52F,
    0x467C,
    {0x8E, 0x3D, 0xC4, 0x57, 0x92, 0x91, 0x69, 0x2E},
};

static const IID IID_IAudioClient_Instance = {
  /* 7ED4EE07-8E67-4CD4-8C1A-2B7A5987AD42 = __uuidof(IAudioClient)  */
    0x7ED4EE07,
    0x8E67,
    0x4CD4,
    {0x8C, 0x1A, 0x2B, 0x7A, 0x59, 0x87, 0xAD, 0x42},
};

static const IID IID_IAudioRenderClient_Instance = {
  /* F294ACFC-3146-4483-A7BF-ADDCA7C260E2 =  __uuidof(IAudioRenderClient)  */

    0xF294ACFC,
    0x3146,
    0x4483,
    {0xA7, 0xBF, 0xAD, 0xDC, 0xA7, 0xC2, 0x60, 0xE2},
};

static const IID IID_IAudioStreamVolume_Instance = {
  /* 93014887-242D-4068-8A15-CF5E93B90FE3 =  __uuidof(IAudioStreamVolume)  */

    0x93014887,
    0x242D,
    0x4068,
    {0x8A, 0x15, 0xCF, 0x5E, 0x93, 0xB9, 0x0F, 0xE3},
};

static const IID IID_IAudioClock_instance = {
  /* CD63314F-3FBA-4A1B-812C-EF96358728E7 =  __uuidof(IID_IAudioClock)  */

    0xCD63314F,
    0x3FBA,
    0x4A1B,
    {0x81, 0x2C, 0xEF, 0x96, 0x35, 0x87, 0x28, 0xE7}
};

#if 0
    static const IID IID_IAudioSessionManager_Instance = {

  /* BFA971F1-4D5E-40BB-935E-967039BFBEE4 = __uuidof(IAudioSessionManager)  */

        0xBFA971F1,
        0x4D5E,
        0x40BB,
        {0x93, 0x5E, 0x96, 0x70, 0x39, 0xBF, 0xBE, 0xE4}
    };
#endif

//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

#endif /* WIN32_CORE_AUDIO_CONTEXT */

/*
You can choose one of these 2 licenses:
-------------------------------------------------------------------------------------
Alternative A: The MIT License (MIT)
Copyright © 2023 Daniel Martins da Silva

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

-------------------------------------------------------------------------------------
Alternative B: BSD Zero Clause License (0BSD)
Copyright (C) 2023 by Daniel Martins da Silva <daaniel_martins@hotmail.com>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
*/
