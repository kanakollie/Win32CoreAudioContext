#ifndef H_GODEN_LOGGER_INCLUDED
  #define H_GODEN_LOGGER_INCLUDED
  /**************************************************************/
  // For variable arguments
  #include <stdarg.h>

  // For size_t
  #include <stddef.h>
/**************************************************************/

  #ifndef GDL_PLATFORM_WIN32
    #if defined(_WIN32) || defined(__WIN32__) || defined(WIN32) || defined(__MINGW32__)
      #define GDL_PLATFORM_WIN32 1
    #else
      #define GDL_PLATFORM_WIN32 0
    #endif
  #endif /* GDL_PLATFORM_WIN32 */

  #ifndef GDL_PLATFORM_LINUX
    #if defined(__linux__)
      #define GDL_PLATFORM_LINUX 1
    #else
      #define GDL_PLATFORM_LINUX 0
    #endif
  #endif /* GDL_PLATFORM_LINUX */

  #define GDL_MAX_TYPE_NAME 16

/**************************************************************/

enum GdlColorENUM {

    GDL_COLOR_RESET,
    GDL_COLOR_BLACK,
    GDL_COLOR_RED,
    GDL_COLOR_GREEN,
    GDL_COLOR_YELLOW,
    GDL_COLOR_BLUE,
    GDL_COLOR_MAGENTA,
    GDL_COLOR_CYAN,
    GDL_COLOR_WHITE,
    GDL_COLOR_LIGHT_BLACK,
    GDL_COLOR_LIGHT_RED,
    GDL_COLOR_LIGHT_GREEN,
    GDL_COLOR_LIGHT_YELLOW,
    GDL_COLOR_LIGHT_BLUE,
    GDL_COLOR_LIGHT_MAGENTA,
    GDL_COLOR_LIGHT_CYAN,
    GDL_COLOR_LIGHT_WHITE,
    GDL_COLOR_COUNT
};

typedef enum GdlColorENUM GdlColors;

struct GdlTypeColorPairSTR {

    const GdlColors Foreground;
    const GdlColors Background;
};

typedef struct GdlTypeColorPairSTR GdlTypeColorPair;

enum GdlLogLevel {

    GDL_LEVEL_DEFAULT,
    GDL_LEVEL_FATAL,
    GDL_LEVEL_ERROR,
    GDL_LEVEL_WARNING,
    GDL_LEVEL_INFO,
    GDL_LEVEL_DEBUG,
    GDL_LEVEL_VERBOSE,
    GDL_LEVEL_COUNT
};

struct GdlLogTypeSTR {

    size_t Level;
    char Name[GDL_MAX_TYPE_NAME];
    struct GdlTypeColorPairSTR Colors;
};

typedef struct GdlLogTypeSTR GdlLogType;

/*============================================================*/

void gdlLogSection(enum GdlLogLevel Level, unsigned int size, unsigned int lineNumber, const char *file_name, const char *text);
void gdlSetConsoleColors(struct GdlTypeColorPairSTR colors);
static void gdlResetConsoleColors(void);

void gdlGetTimeString(char *timeString, unsigned long timeStringSize);
void gdlLog(enum GdlLogLevel Level, unsigned int line, const char *file_name, const char *message, ...);

/*============================================================*/

  #ifndef GDL_ARRAY_SIZE
    #define GDL_ARRAY_SIZE(ARRAY) (sizeof(ARRAY) / sizeof(ARRAY[0]))
  #endif

  #ifndef GDL_IS_VALID_LOG_TYPE
    #define GDL_IS_VALID_LOG_TYPE(LogType) ((LogType).Level < GDL_LEVEL_COUNT)
  #endif

  #ifdef GDL_HIDE_LINE_NUMBER
    #define GDL_LINE_NUMBER 0
  #else
    #define GDL_LINE_NUMBER (__LINE__)
  #endif

  #ifdef GDL_HIDE_FILE_NAME
    #define GDL_FILE_NAME 0
  #else
    #include <string.h>
    #if (GDL_PLATFORM_WIN32 == 1)
      #define GDL_FILE_NAME (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)
    #else
      #define GDL_FILE_NAME (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
    #endif
  #endif

  #define GDL_LOG_SECTION(LEVEL, SIZE, TEXT)                                                       \
        gdlLogSection(LEVEL, SIZE, GDL_LINE_NUMBER, GDL_FILE_NAME, TEXT)

  #define GDL_LOG_MESSAGE(LEVEL, ...)                                                              \
        do {                                                                                       \
            gdlLog(LEVEL, GDL_LINE_NUMBER, GDL_FILE_NAME, __VA_ARGS__);                            \
            printf("\n");                                                                          \
        } while (0)

  #ifdef GDL_LEVEL_DISABLE_FATAL
    #define GDL_FATAL(...)
    #define GDL_SECTION_FATAL(TEXT, SIZE)
  #else
    #define GDL_FATAL(...)                GDL_LOG_MESSAGE(GDL_LEVEL_FATAL, __VA_ARGS__)
    #define GDL_SECTION_FATAL(TEXT, SIZE) GDL_LOG_SECTION(GDL_LEVEL_FATAL, SIZE, TEXT)
  #endif

  #ifdef GDL_LEVEL_DISABLE_ERROR
    #define GDL_ERROR(...)
    #define GDL_SECTION_ERROR(TEXT, SIZE)
  #else
    #define GDL_ERROR(...)                GDL_LOG_MESSAGE(GDL_LEVEL_ERROR, __VA_ARGS__)
    #define GDL_SECTION_ERROR(TEXT, SIZE) GDL_LOG_SECTION(GDL_LEVEL_ERROR, SIZE, TEXT)
  #endif

  #ifdef GDL_LEVEL_DISABLE_WARNING
    #define GDL_WARNING(...)
    #define GDL_SECTION_WARNING(TEXT, SIZE)
  #else
    #define GDL_WARN(...)                GDL_LOG_MESSAGE(GDL_LEVEL_WARNING, __VA_ARGS__)
    #define GDL_SECTION_WARN(TEXT, SIZE) GDL_LOG_SECTION(GDL_LEVEL_WARNING, SIZE, TEXT)
  #endif

  #ifdef GDL_LEVEL_DISABLE_INFO
    #define GDL_INFO(...)
    #define GDL_SECTION_INFO(TEXT, SIZE)
  #else
    #define GDL_INFO(...)                GDL_LOG_MESSAGE(GDL_LEVEL_INFO, __VA_ARGS__)
    #define GDL_SECTION_INFO(TEXT, SIZE) GDL_LOG_SECTION(GDL_LEVEL_INFO, SIZE, TEXT)
  #endif

  #ifdef GDL_LEVEL_DISABLE_DEBUG
    #define GDL_DEBUG(...)
    #define GDL_SECTION_DEBUG(TEXT, SIZE)
  #else
    #define GDL_DEBUG(...)                GDL_LOG_MESSAGE(GDL_LEVEL_DEBUG, __VA_ARGS__)
    #define GDL_SECTION_DEBUG(TEXT, SIZE) GDL_LOG_SECTION(GDL_LEVEL_DEBUG, SIZE, TEXT)
  #endif

  #ifdef GDL_LEVEL_DISABLE_VERBOSE
    #define GDL_VERBOSE(...)
    #define GDL_SECTION_VERBOSE(TEXT, SIZE)
  #else
    #define GDL_VERBOSE(...)                GDL_LOG_MESSAGE(GDL_LEVEL_VERBOSE, __VA_ARGS__)
    #define GDL_SECTION_VERBOSE(TEXT, SIZE) GDL_LOG_SECTION(GDL_LEVEL_VERBOSE, SIZE, TEXT)
  #endif

  #ifdef GDL_LEVEL_DISABLE_DEFAULT
    #define GDL_DEFAULT(...)
    #define GDL_SECTION_DEFAULT(TEXT, SIZE)
  #else
    #define GDL_DEFAULT(...)                GDL_LOG_MESSAGE(GDL_LEVEL_DEFAULT, __VA_ARGS__)
    #define GDL_SECTION_DEFAULT(TEXT, SIZE) GDL_LOG_SECTION(GDL_LEVEL_DEFAULT, SIZE, TEXT)
  #endif

/**************************************************************/
#endif // H_GODEN_LOGGER_INCLUDED
/**************************************************************/

#if !defined(C_GODEN_LOGGER_INCLUDED) && defined(GD_LOGGER_IMPLEMENTATION)
  #define C_GODEN_LOGGER_INCLUDED
/**************************************************************/

  #include <stdio.h>
  #include <string.h>

  #if (GDL_PLATFORM_WIN32 == 1)

    #include <Windows.h>

    // TODO: When compiling for windows use platform specific code to get the time?
    #include <time.h>

  #elif (GDL_PLATFORM_LINUX == 1)
    #include <time.h>
  #else
    #include <time.h>
  #endif

/*============================================================*/

// COMPILER SPECIFIC

  #ifdef __clang__

    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Wformat-nonliteral"

  #endif

// COMPILER SPECIFIC -- END

static const GdlLogType GDL_LOG_TYPES[GDL_LEVEL_COUNT] = {

    [GDL_LEVEL_DEFAULT] = {

            .Level = GDL_LEVEL_DEFAULT,
            .Name = "DEFAULT",
            .Colors.Foreground = GDL_COLOR_LIGHT_WHITE,
            .Colors.Background = GDL_COLOR_RESET,
        },

    [GDL_LEVEL_FATAL] = {

            .Level = GDL_LEVEL_FATAL,
            .Name = "FATAL",
            .Colors.Foreground = GDL_COLOR_LIGHT_WHITE,
            .Colors.Background = GDL_COLOR_RED,
        },

    [GDL_LEVEL_ERROR] = {

            .Level = GDL_LEVEL_ERROR,
            .Name = "ERROR",
            .Colors.Foreground = GDL_COLOR_LIGHT_RED,
            .Colors.Background = GDL_COLOR_RESET,
        },

    [GDL_LEVEL_WARNING] = {

            .Level = GDL_LEVEL_WARNING,
            .Name = "WARNING",
            .Colors.Foreground = GDL_COLOR_LIGHT_YELLOW,
            .Colors.Background = GDL_COLOR_RESET,
        },

    [GDL_LEVEL_INFO] = {

            .Level = GDL_LEVEL_INFO,
            .Name = "INFO",
            .Colors.Foreground = GDL_COLOR_LIGHT_WHITE,
            .Colors.Background = GDL_COLOR_RESET,
        },

    [GDL_LEVEL_DEBUG] = {

            .Level = GDL_LEVEL_DEBUG,
            .Name = "DEBUG",
            .Colors.Foreground = GDL_COLOR_CYAN,
            .Colors.Background = GDL_COLOR_RESET,
        },

    [GDL_LEVEL_VERBOSE] = {
      
            .Level = GDL_LEVEL_VERBOSE,
            .Name = "VERBOSE",
            .Colors.Foreground = GDL_COLOR_LIGHT_BLACK,
            .Colors.Background = GDL_COLOR_RESET,
        },
};

void gdlSetConsoleColors(struct GdlTypeColorPairSTR colors) {

  #if (GDL_PLATFORM_WIN32 == 1)

    static const WORD backgroundColors[GDL_COLOR_COUNT] = {

        [GDL_COLOR_RESET] = 0,
        [GDL_COLOR_BLACK] = 0,
        [GDL_COLOR_RED] = BACKGROUND_RED,
        [GDL_COLOR_GREEN] = BACKGROUND_GREEN,
        [GDL_COLOR_YELLOW] = BACKGROUND_GREEN | BACKGROUND_RED,
        [GDL_COLOR_BLUE] = BACKGROUND_BLUE,
        [GDL_COLOR_MAGENTA] = BACKGROUND_BLUE | BACKGROUND_RED,
        [GDL_COLOR_CYAN] = BACKGROUND_BLUE | BACKGROUND_GREEN,
        [GDL_COLOR_WHITE] = BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED,

        [GDL_COLOR_LIGHT_BLACK] = BACKGROUND_INTENSITY,
        [GDL_COLOR_LIGHT_GREEN] = BACKGROUND_GREEN | BACKGROUND_INTENSITY,
        [GDL_COLOR_LIGHT_RED] = BACKGROUND_RED | BACKGROUND_INTENSITY,
        [GDL_COLOR_LIGHT_YELLOW] = BACKGROUND_GREEN | BACKGROUND_RED | BACKGROUND_INTENSITY,
        [GDL_COLOR_LIGHT_BLUE] = BACKGROUND_BLUE | BACKGROUND_INTENSITY,
        [GDL_COLOR_LIGHT_MAGENTA] = BACKGROUND_BLUE | BACKGROUND_RED | BACKGROUND_INTENSITY,
        [GDL_COLOR_LIGHT_CYAN] = BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_INTENSITY,
        [GDL_COLOR_LIGHT_WHITE] = BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED | BACKGROUND_INTENSITY,

    };

    static const WORD foregroundColors[GDL_COLOR_COUNT] = {

        [GDL_COLOR_BLACK] = 0,
        [GDL_COLOR_RED] = FOREGROUND_RED,
        [GDL_COLOR_GREEN] = FOREGROUND_GREEN,
        [GDL_COLOR_YELLOW] = FOREGROUND_GREEN | FOREGROUND_RED,
        [GDL_COLOR_BLUE] = FOREGROUND_BLUE,
        [GDL_COLOR_MAGENTA] = FOREGROUND_BLUE | FOREGROUND_RED,
        [GDL_COLOR_CYAN] = FOREGROUND_BLUE | FOREGROUND_GREEN,
        [GDL_COLOR_WHITE] = FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED,

        [GDL_COLOR_LIGHT_BLACK] = FOREGROUND_INTENSITY,
        [GDL_COLOR_LIGHT_GREEN] = FOREGROUND_GREEN | FOREGROUND_INTENSITY,
        [GDL_COLOR_LIGHT_RED] = FOREGROUND_RED | FOREGROUND_INTENSITY,
        [GDL_COLOR_LIGHT_YELLOW] = FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY,
        [GDL_COLOR_LIGHT_BLUE] = FOREGROUND_BLUE | FOREGROUND_INTENSITY,
        [GDL_COLOR_LIGHT_MAGENTA] = FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY,
        [GDL_COLOR_LIGHT_CYAN] = FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_INTENSITY,
        [GDL_COLOR_LIGHT_WHITE] = FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY,
    };

    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    WORD new_attributes;
    new_attributes = backgroundColors[colors.Background] | foregroundColors[colors.Foreground];
    SetConsoleTextAttribute(hConsole, new_attributes);

  #elif (GDL_PLATFORM_LINUX == 1)

    static const unsigned char foregroundColors[GDL_COLOR_COUNT] = {

        [GDL_COLOR_RESET] = 0,         [GDL_COLOR_BLACK] = 30,       [GDL_COLOR_RED] = 31,
        [GDL_COLOR_GREEN] = 32,        [GDL_COLOR_YELLOW] = 33,      [GDL_COLOR_BLUE] = 34,
        [GDL_COLOR_MAGENTA] = 35,      [GDL_COLOR_CYAN] = 36,        [GDL_COLOR_WHITE] = 37,
        [GDL_COLOR_LIGHT_BLACK] = 90,  [GDL_COLOR_LIGHT_RED] = 91,   [GDL_COLOR_LIGHT_GREEN] = 92,
        [GDL_COLOR_LIGHT_YELLOW] = 93, [GDL_COLOR_LIGHT_BLUE] = 94,  [GDL_COLOR_LIGHT_MAGENTA] = 95,
        [GDL_COLOR_LIGHT_CYAN] = 96,   [GDL_COLOR_LIGHT_WHITE] = 97,
    };

    static const unsigned char backgroundColors[GDL_COLOR_COUNT] = {

        [GDL_COLOR_RESET] = 0,
        [GDL_COLOR_BLACK] = 40,
        [GDL_COLOR_RED] = 41,
        [GDL_COLOR_GREEN] = 42,
        [GDL_COLOR_YELLOW] = 44,
        [GDL_COLOR_BLUE] = 44,
        [GDL_COLOR_MAGENTA] = 45,
        [GDL_COLOR_CYAN] = 46,
        [GDL_COLOR_WHITE] = 47,
        [GDL_COLOR_LIGHT_BLACK] = 100,
        [GDL_COLOR_LIGHT_RED] = 101,
        [GDL_COLOR_LIGHT_GREEN] = 102,
        [GDL_COLOR_LIGHT_YELLOW] = 104,
        [GDL_COLOR_LIGHT_BLUE] = 104,
        [GDL_COLOR_LIGHT_MAGENTA] = 105,
        [GDL_COLOR_LIGHT_CYAN] = 106,
        [GDL_COLOR_LIGHT_WHITE] = 107,
    };

    printf("\x1B[%um", backgroundColors[colors.Background]);
    printf("\x1B[%um", foregroundColors[colors.Foreground]);

  #else
        // Platform not supported
  #endif
}

inline void gdlResetConsoleColors(void) {
    gdlSetConsoleColors(GDL_LOG_TYPES[GDL_LEVEL_DEFAULT].Colors);
}

void gdlGetTimeString(char *timeString, unsigned long timeStringSize) {

    const time_t localTime = time(NULL);
    if (timeStringSize < 9) {

        printf("'timeStringSize' should be at least 9\n");
    }

  #if (GDL_PLATFORM_WIN32 == 1)

    struct tm calendarDate;
    errno_t result = localtime_s(&calendarDate, &localTime);
    if (result != 0) {

        printf("Error %d in 'localtime_s'\n", result);
    }

    strftime(timeString, timeStringSize, "%T", &calendarDate);

  #elif (GDL_PLATFORM_LINUX == 1)

    struct tm *calendarDate = localtime(&localTime);
    strftime(timeString, timeStringSize, "%T", calendarDate);

  #else

    struct tm *calendarDate = localtime(&localTime);
    strftime(timeString, timeStringSize, "%T", calendarDate);

  #endif
}

static void gdlGetPaddingString(const GdlLogType *logType, char *paddingString, const unsigned int paddingStringSize) {

    if (paddingStringSize < GDL_MAX_TYPE_NAME) {

        printf("Padding string size must be at least the size of 'GDL_MAX_TYPE_NAME'");
        return;
    }

    size_t paddingStringEnd = 0;
    if (logType != NULL) {

        size_t typeNameSize = 0;
        for (size_t i = 0; i < GDL_LEVEL_COUNT; ++i) {

            typeNameSize = strlen(GDL_LOG_TYPES[i].Name);
            if (typeNameSize > paddingStringEnd) {

                paddingStringEnd = typeNameSize - 1;
            }
        }

        const size_t logTypeStringSize = (unsigned int)strlen(logType->Name);
        if (logTypeStringSize <= typeNameSize) {

            paddingStringEnd = typeNameSize - logTypeStringSize;
            for (size_t i = 0; i < paddingStringEnd; ++i) {

                paddingString[i] = 32;
            }

        } else {

            gdlSetConsoleColors(GDL_LOG_TYPES[GDL_LEVEL_ERROR].Colors);
            printf("(gdlLogMessage) 'logTypeString' should not be greater than 'paddingString'\n");
            gdlSetConsoleColors(logType->Colors);
        }

        paddingString[paddingStringEnd] = '\0';
    }
}

void gdlLogSection(const enum GdlLogLevel Level,
                   unsigned int size,
                   unsigned int lineNumber,
                   const char *file_name,
                   const char *text) {

    printf("\n");

    const GdlLogType *logType = &GDL_LOG_TYPES[Level];
    if (Level < GDL_LEVEL_COUNT) {
        gdlSetConsoleColors(logType->Colors);
    }

    unsigned long i = 0;
    while (i++ < size) {

        printf("=");
    }

    printf(" ");

    if (Level < GDL_LEVEL_COUNT) {

  #if 1 // Enable to show the log  type name   char typeName[GDL_MAX_TYPE_NAME];

        if (logType->Level != GDL_LEVEL_DEFAULT) {

            printf("[%s] ", logType->Name);
            char paddingString[GDL_MAX_TYPE_NAME];
            paddingString[0] = '\0';
            gdlGetPaddingString(logType, paddingString, GDL_ARRAY_SIZE(paddingString));
            printf("%s", paddingString);
        }
  #endif

    } else {

        printf("[INVALID] ");
    }

    printf("%s ", text);

    if (lineNumber > 0 && file_name != NULL) {

        printf(":: %s:%u ", file_name, lineNumber);

    } else if (lineNumber > 0) {

        printf(":: %u ", lineNumber);

    } else if (file_name != NULL) {

        printf(":: %s ", file_name);
    }

    while (i-- > 0) {

        printf("=");
    }

    gdlResetConsoleColors();
    printf("\n\n");
}

void gdlLog(enum GdlLogLevel Level, unsigned int line, const char *file_name, const char *message, ...) {

    char timeString[10] = { [9] = '\0' };

    const GdlLogType *logType = &GDL_LOG_TYPES[Level];
    gdlGetTimeString(timeString, sizeof(timeString) - 1);
    gdlSetConsoleColors(logType->Colors);

    if (Level < GDL_LEVEL_COUNT) {

        char paddingString[GDL_MAX_TYPE_NAME];
        paddingString[0] = '\0';

        gdlGetPaddingString(logType, paddingString, GDL_ARRAY_SIZE(paddingString));
        printf("[%s] ", timeString);
        if (logType->Level != GDL_LEVEL_DEFAULT) {

            printf("[%s]%s ", logType->Name, paddingString);
        }

    } else {

        printf("[%s] [INVALID] ", timeString);
    }

    if (line > 0 && file_name != NULL) {

        printf("[%s:%u] ", file_name, line);

    } else if (line > 0) {

        printf("[%u] ", line);

    } else if (file_name != NULL) {

        printf("[%s] ", file_name);
    }

    va_list args;
    va_start(args, message);
    vprintf(message, args);
    va_end(args);

    gdlResetConsoleColors();
}

  #ifdef __clang__
    #pragma clang diagnostic pop
  #endif

/**************************************************************/
#endif // !defined(C_GODEN_LOGGER_INCLUDED) && defined(GD_LOGGER_IMPLEMENTATION)

/*

You can choose one of these 2 licenses:
-------------------------------------------------------------------------------------
Alternative A: The MIT License (MIT)
Copyright © 2023 Daniel Martins da Silva

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

-------------------------------------------------------------------------------------
Alternative B: BSD Zero Clause License (0BSD)
Copyright (C) 2023 by Daniel Martins da Silva <daaniel_martins@hotmail.com>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.

*/
